package com.blog.domain.comment;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.domain.post.Post;
import com.blog.domain.post.PostRepository;

@Service
public class CommentService {
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private CommentRepository commentRepository;

	/**
	 * Returns a list of all comments for a blog post with passed id.
	 *
	 * @param postId id of the post
	 * @return list of comments sorted by creation date descending - most recent first
	 * @throws IllegalArgumentException if there is no blog post for passed postId
	 */
	public List<CommentDto> getCommentsForPost(Long postId) {
		return postRepository.findById(postId).map(post -> CommentDto.parse(post.getComments())).orElseThrow(() -> new IllegalArgumentException("Post not found"));
	}

	/**
	 * Creates a new comment
	 *
	 * @param newCommentDto data of new comment
	 * @return id of created comment
	 * @throws IllegalArgumentException if there is no blog post for passed newCommentDto.postId
	 */
	
	public Long addComment(NewCommentDto newCommentDto) {
		Post post = postRepository.findById(newCommentDto.getPostId()).orElseThrow(() -> new IllegalArgumentException("Post not found"));
//		CommentDto comment = new CommentDto(new SecureRandom().nextLong(), newCommentDto.getContent(), newCommentDto.getAuthor(), LocalDate.now(), post);
		
		Comment comment = new Comment();
		comment.setAuthor(newCommentDto.getAuthor());
		comment.setComment(newCommentDto.getContent());
		comment.setCreationDate(LocalDate.now());
		comment.setPost(post);
		
		comment = commentRepository.save(comment);
		
		//CommentDto commentDto = new CommentDto(new Comment());
		post.getComments().add(comment);
		postRepository.save(post);
		return comment.getId();
	}
}
