package com.blog.domain.comment;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.blog.domain.post.Post;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CommentDto {

	private final Long id;
	private final String comment;
	private final String author;
	
	@CreationTimestamp
	private final LocalDate creationDate;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "post_id")
	private Post post;
	
	public CommentDto(Comment comment) {
		this.id = comment.getId();
		this.comment = comment.getComment();
		this.author = comment.getAuthor();
		this.creationDate = comment.getCreationDate();
	}
	
	public static List<CommentDto> parse(List<Comment> comment) {
		return comment.stream().map(c -> new CommentDto(c)).collect(Collectors.toList());
	}

}
