package com.blog.domain.comment;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class NewCommentDto {

	@NotNull(message = "PostId cannot be null!")
	private Long postId;
	
	@NotBlank(message = "Author cannot be null!")
	private String author;
	
	@NotBlank(message = "Content cannot be null!")
	private String content;

}
