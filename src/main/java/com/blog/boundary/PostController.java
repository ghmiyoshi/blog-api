package com.blog.boundary;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.blog.domain.comment.CommentDto;
import com.blog.domain.comment.CommentService;
import com.blog.domain.comment.NewCommentDto;
import com.blog.domain.post.PostDto;
import com.blog.domain.post.PostService;

@RestController
@RequestMapping("/post")
public class PostController {

	private final PostService postService;
	private final CommentService commentService;

	public PostController(PostService postService, CommentService commentService) {
		this.postService = postService;
		this.commentService = commentService;
	}

	@GetMapping("/{id}")
	public Optional<PostDto> getPost(@PathVariable Long id) {
		return Optional.of(postService.getPost(id).orElseThrow(() -> new ResourceNotFoundException("Resource Not Found")));
	}

	@GetMapping("/{id}/comments")
	public List<CommentDto> getComments(@PathVariable Long id) {
		try {
			return commentService.getCommentsForPost(id);
		} catch (IllegalArgumentException e) {
			throw new ResourceNotFoundException(e.getMessage());
		}
	}

	@PostMapping("/{id}/comment")
	@ResponseStatus(HttpStatus.CREATED)
	public void addComment(@PathVariable Long id, @Valid @RequestBody NewCommentDto newCommentDto) {
		newCommentDto.setPostId(id);
		commentService.addComment(newCommentDto);
	}

	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	class ResourceNotFoundException extends RuntimeException {

		private static final long serialVersionUID = 1L;
		
		public ResourceNotFoundException(String message) {
			super(message);
		}
	}
}
